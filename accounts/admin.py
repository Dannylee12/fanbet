from django.contrib import admin
from accounts.models import (PLGame, Profile, Transaction, PLResults, BetForm,
                             GamePool, Sports, Team, Pools)
# Register your models here.

admin.site.register(PLGame)
admin.site.register(PLResults)
admin.site.register(Profile)
admin.site.register(Transaction)
admin.site.register(BetForm)
admin.site.register(GamePool)
admin.site.register(Sports)
admin.site.register(Team)
admin.site.register(Pools)
