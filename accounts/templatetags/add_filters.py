from django import template

register = template.Library()


@register.filter(name='filter_odds')
def filter_odds(value, arg):
    """
    Strips off all trailing 0s in a decimal.
    arg is the number of decimal places to return (maximum)
    i.e: filter_odds(1.234) -> 1.23
         filter_odds(1.200) -> 1.2
    """
    if not value:
        return ""
    if value == int(value):
        return int(value)
    # Floating point
    new_val = str(value).split(".")[0] + "."
    decimal = str(value).split(".")[1]
    if len(decimal) <= arg:
        return value
    new_val += decimal[:arg] if int(decimal[arg]) < 5 \
        else decimal[:arg-1] + str(int(decimal[arg-1])+1)
    return new_val
