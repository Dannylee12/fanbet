import datetime

from django.db import models
from django.contrib.postgres.fields import ArrayField
from django.contrib.auth.models import User
from django.utils import timezone


class Sports(models.Model):
    """
    List of sports
    """
    sport_name = models.CharField(choices=(('PL_FOOTBALL',
                                            'premier_league_football'),
                                           ('SUPER_RUGBY', 'superugby')),
                                  max_length=20)
    added = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.sport_name


class Team(models.Model):
    """
    List of teams:
    Each user must choose a team when they join. Then they are placed into that
    team's GamePool automatically.
    """
    sport = models.ForeignKey(Sports, on_delete=models.CASCADE)
    team_name = models.CharField(max_length=30)
    added = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.team_name


class GamePool(models.Model):
    """
    Contains all of the pools, at the very least all users have Global pool and 
    supporters pool.
    links to Profile, Sport
    """
    sport = models.ForeignKey(Sports, on_delete=models.CASCADE)
    pool_name = models.CharField(max_length=100)
    is_supporters_pool = models.BooleanField(default=False)
    # If it's a supporters pool, it's linked to a PL team
    supporters_pool = models.ForeignKey(Team, null=True, blank=True,
                                        on_delete=models.CASCADE)
    is_private_pool = models.BooleanField(default=True)
    # season = models.CharField(max_length=20, default='2018/2019')
    added = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.pool_name


class Profile(models.Model):
    """
    Extends User model with various aspects that are required.
    """
    # Inherit from User
    user = models.OneToOneField(User, models.CASCADE)
    supported_team = models.ForeignKey(Team, default=1, on_delete=models.CASCADE)
    # Account balance in Game dollars
    game_account_balance = models.IntegerField(default=1000)
    game_account_accrued_balance = models.IntegerField(default=0)
    # Previous weeks total - determine payout
    previous_payment = models.IntegerField(default=1000)
    end_of_previous_week_total = models.FloatField(default=0)
    weeks_profit = models.FloatField(default=0)
    # All rankings to be stored so they can be rendered instantly.
    global_ranking = models.IntegerField(null=True, blank=True)
    global_ranking_last_week = models.IntegerField(null=True, blank=True)
    supporter_pool = models.ForeignKey(GamePool, null=True, blank=True,
                                       on_delete=models.CASCADE)
    supporter_pool_ranking = models.IntegerField(null=True, blank=True)
    supporter_pool_ranking_last_week = models.IntegerField(null=True,
                                                           blank=True)
    pools = models.ManyToManyField(GamePool, blank=True, related_name='pools')
    pool_rankings = models.CharField(max_length=500, null=True, default=True)
    pool_rankings_last_week = models.CharField(max_length=500, null=True,
                                               default=True)
    # Optional fields - Bank details
    bank_name = models.CharField(max_length=20, blank=True, null=True,
                                 help_text='Fields below not required')
    bank_account_number = models.IntegerField(blank=True, null=True)
    branch_code = models.IntegerField(blank=True, null=True)
    bank_balance = models.IntegerField(blank=True, null=True)
    added = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.user.username


class PLGame(models.Model):
    """
    Premier league game.
    Threeway, Double Chance, Player sent off and Both teams to score.
    """
    home_team = models.ForeignKey(Team, related_name='Home',
                                  on_delete=models.CASCADE)
    away_team = models.ForeignKey(Team, on_delete=models.CASCADE)
    # Threeway
    tw_home = models.FloatField()
    tw_away = models.FloatField()
    tw_draw = models.FloatField()
    # Double Chance
    dc_home_away = models.FloatField()
    dc_home_draw = models.FloatField()
    dc_away_draw = models.FloatField()
    # Player sent off (Red card)
    so = models.FloatField(default=None, null=True, blank=True)  # TODO: Delete
    # Total Goals
    over05 = models.FloatField()
    under05 = models.FloatField()
    over15 = models.FloatField()
    under15 = models.FloatField()
    over25 = models.FloatField()
    under25 = models.FloatField()
    over35 = models.FloatField()
    under35 = models.FloatField()
    # BTTS
    btts_yes = models.FloatField()
    btts_no = models.FloatField()

    # Season
    season = models.CharField(max_length=20)

    passed = models.BooleanField(default=False)

    date = models.DateTimeField(default=datetime.datetime(1970, 1, 1))
    added = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-date']

    def __str__(self):
        return self.home_team.team_name + ' vs ' + self.away_team.team_name


class PLResults(models.Model):
    game = models.OneToOneField(PLGame, on_delete=models.CASCADE)
    # Three way
    tw_home = models.BooleanField(default=False)
    tw_away = models.BooleanField(default=False)
    tw_draw = models.BooleanField(default=False)
    # Double Chance
    dc_home_away = models.BooleanField(default=False)
    dc_home_draw = models.BooleanField(default=False)
    dc_away_draw = models.BooleanField(default=False)
    # Both Teams to Score
    btts_yes = models.BooleanField(default=False)
    btts_no = models.BooleanField(default=False)
    # Total goals
    over05 = models.BooleanField(default=False)
    under05 = models.BooleanField(default=False)
    over15 = models.BooleanField(default=False)
    under15 = models.BooleanField(default=False)
    over25 = models.BooleanField(default=False)
    under25 = models.BooleanField(default=False)
    over35 = models.BooleanField(default=False)
    under35 = models.BooleanField(default=False)
    added = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    evaluated = models.BooleanField(default=False)
    # TODO: Remove
    so = models.BooleanField(default=False)
    btts = models.BooleanField(default=False)

    class Meta:
        ordering = ['-updated']

    def was_updated_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(minutes=5) <= self.updated <= now
        # return True

    def set_evaluated(self):
        self.evaluated = True
        self.passed = True
        self.save()

    def __str__(self):
        return str(self.game) + 'Results'


class Transaction(models.Model):
    user = models.ForeignKey(Profile, blank=True, null=True,
                             on_delete=models.CASCADE)
    stand_to_win = models.FloatField()
    # To be replaced with a choice field.
    odds = models.FloatField(blank=True, null=True)
    status = models.CharField(max_length=20, choices=(('PENDING', 'pending'),
                                                      ('WIN', 'win'),
                                                      ('LOSE', 'lose')))
    added = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.user.user.username) + str(self.odds) + \
               str(self.stand_to_win)


class BetForm(models.Model):
    transaction = models.ForeignKey(Transaction, blank=True, null=True,
                                    related_name='betform',
                                    on_delete=models.CASCADE)
    user = models.ForeignKey(Profile, blank=True, on_delete=models.CASCADE)
    game = models.ForeignKey(PLGame, on_delete=models.CASCADE)
    bet = models.CharField(max_length=20, choices=(('tw_home', 'tw_home'),
                                                   ('tw_away', 'tw_away'),
                                                   ('tw_draw', 'tw_draw'),
                                                   ('dc_home_away',
                                                    'dc_home_away')))
    odds = models.FloatField()
    bet_placed = models.BooleanField(default=False)
    bet_passed = models.BooleanField(default=False)
    status = models.CharField(max_length=20, choices=(('PENDING', 'pending'),
                                                      ('WIN', 'win'),
                                                      ('LOSE', 'lose')))

    added = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str("Betting form ") + str(self.user.user.username) \
               + ' ' + str(self.transaction_id)


class Pools(models.Model):
    """
    Keeps all pool rankings for each GamePool. Can be used to graph results
    nicely.
    """
    pool_name = models.ForeignKey(GamePool, related_name='pool_name_in_Pools',
                                  on_delete=models.CASCADE)
    member = models.ForeignKey(Profile, related_name='member_in_pool',
                               on_delete=models.CASCADE)  # Person in pool
    rank = models.IntegerField(default=-1)
    accrued_monies = models.IntegerField(default=0)
    date = models.DateField()  # Date when this result is set

    added = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
