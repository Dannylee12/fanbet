# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-10-26 19:48
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0044_auto_20171026_1925'),
    ]

    operations = [
        migrations.AlterField(
            model_name='plresults',
            name='btts',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='plresults',
            name='dc_away_draw',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='plresults',
            name='dc_home_away',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='plresults',
            name='dc_home_draw',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='plresults',
            name='over05',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='plresults',
            name='over15',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='plresults',
            name='over25',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='plresults',
            name='over35',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='plresults',
            name='tw_away',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='plresults',
            name='tw_draw',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='plresults',
            name='tw_home',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='plresults',
            name='under05',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='plresults',
            name='under15',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='plresults',
            name='under25',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='plresults',
            name='under35',
            field=models.BooleanField(default=False),
        ),
    ]
