# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-06-26 19:32
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0031_remove_gamepool_is_global_pool'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='pools',
            field=models.ManyToManyField(blank=True, related_name='pools', to='accounts.GamePool'),
        ),
    ]
