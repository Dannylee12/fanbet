# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-05-17 06:07
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0009_betform_bet'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='transaction',
            name='game',
        ),
    ]
