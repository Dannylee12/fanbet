# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-06-26 18:07
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0027_auto_20170619_1635'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='game_account_accrued_balance',
            field=models.IntegerField(default=0),
        ),
    ]
