# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-06-26 20:04
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0032_auto_20170626_1932'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='profile',
            name='pool_one_ranking',
        ),
        migrations.RemoveField(
            model_name='profile',
            name='pool_three_ranking',
        ),
        migrations.RemoveField(
            model_name='profile',
            name='pool_two_ranking',
        ),
        migrations.AddField(
            model_name='profile',
            name='pool_rankings',
            field=models.CharField(default=True, max_length=300, null=True),
        ),
    ]
