# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-05-23 16:55
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0020_auto_20170523_1654'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='supported_team',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='accounts.Team'),
        ),
    ]
