from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist

from accounts.models import Profile, PLGame


class UserRegistrationForm(UserCreationForm):
    # password1 = forms.CharField(widget=forms.PasswordInput())
    # confirm_password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2')

    def clean(self):
        cleaned_data = super(UserRegistrationForm, self).clean()
        password = cleaned_data.get("password1")
        confirm_password = cleaned_data.get("password2")

        if password != confirm_password:
            raise forms.ValidationError(
                "Error: Passwords do not match!"
            )

        try:
            User.objects.get(email=cleaned_data.get('email'))
            raise forms.ValidationError(
                {"email":
                     "Email address is already in use. Please reset your account"}
            )
        except ObjectDoesNotExist:
            pass

        # Validate that the username is not taken
        try:
            User.objects.get(username=cleaned_data.get("username"))
            raise forms.ValidationError(
                "Username is already taken. Please choose another"
            )
        except ObjectDoesNotExist:
            pass


class UserLoginForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta():
        model = User
        fields = ('username', 'password')


class ProfileForm(forms.ModelForm):
    """
    Used to get all other fields not in UserForm
    """

    class Meta:
        model = Profile
        fields = ['supported_team', 'bank_name', 'bank_account_number',
                  'branch_code', 'bank_balance']


class BetForm(forms.ModelForm):
    """ Where bets can be placed. """
    class Meta:
        model = PLGame
        fields = '__all__'
