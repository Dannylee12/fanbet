from django.db.models.signals import post_save
from django.dispatch import receiver
from accounts.tasks import (evaluate_bet_results, order_global_pool,
                            order_supporter_pools, get_pool_rankings)
from accounts.models import PLResults, Profile, BetForm


import logging

logging.basicConfig(level='INFO')
logger = logging.getLogger()


@receiver(post_save, sender=PLResults)
def evaluate_bets(sender, instance, created, **kwargs):
    if created:
        logger.info(f"instance {instance} sent by {sender}")
        # Order global Pool
        order_global_pool_task = order_global_pool.delay()
        # Order Supporter Pool
        order_supporter_pool_task = order_supporter_pools.delay()
        # Order all users in all pools
        for i, user in enumerate(Profile.objects.all()):
            i = get_pool_rankings.delay(user.user.username)
        # Evaluate bets
        # Get all betforms that have this game in them
        betforms = BetForm.objects.filter(game=instance.game)
        for betform in betforms:
            # Group by Transaction
            transaction = betform.transaction
            logger.info("Transaction: " + str(transaction))
            win = True
            forms = BetForm.objects.filter(transaction=betform.transaction)
            for form in forms:
                logger.info(f"form: {form}")
                if not form.game.passed:
                    logger.info("all games not finished")
                    win = False
                    break
                result = PLResults.objects.get(game=form.game)
                logger.info("Result from: " + str(result))
                logger.info("WIN if this is true: " + str(form.bet))
                logger.info(getattr(result, form.bet))
                if getattr(result, form.bet):
                    logger.info('bet won')
                    form.status = 'WIN'
                else:
                    logger.info ("bet lost")
                    form.status = 'LOSE'
                    transaction.status = 'LOSE'
                    transaction.save()
                    win = False
                form.bet_passed = True
                form.save()
            if win:
                logger.info("pay_winners")
                pay_winners(transaction)
                transaction.status = 'WIN'
                transaction.save()
                logger.info("Set Evaluated")
        instance.set_evaluated()
        logger.info('instance saved')
        return True
    else:
        logger.error(f"Instance: {instance} not created.")
        return True


def game_passed(instance):
    """
    Log that the game has passed.
    :param instance: PLResult object
    :return: None
    """
    game = instance.game
    game.passed = True
    game.save()


def pay_winners(transaction):
    """
    Pay winners if their bets are all winners.
    :param transaction: winning transactions.
    :return: None
    """
    profile = Profile.objects.get(user=transaction.user.user)
    profile.game_account_accrued_balance += transaction.stand_to_win
    profile.save()
