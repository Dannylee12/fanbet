from django.test import TestCase, RequestFactory
from django.contrib.auth.models import AnonymousUser, User
from django.urls import reverse
from django.test.client import Client

from .models import PLResults, PLGame, Team, Sports, User, Profile, GamePool
from .views import append_bet, user_login
from .tasks import fraction_to_float


class TestPLResults(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.client = Client()

        sport = Sports.objects.create(sport_name='PL_FOOTBALL')
        home = Team.objects.create(sport=sport, team_name='Home')
        # home2 = Team.objects.create(sport=sport, team_name='Home2')
        # home3 = Team.objects.create(sport=sport, team_name='Home3')
        # away = Team.objects.create(sport=sport, team_name='Away')
        # away2 = Team.objects.create(sport=sport, team_name='Away2')
        # away3 = Team.objects.create(sport=sport, team_name='Away3')
        # self.game = PLGame.objects.create(home_team=home, away_team=away,
        #                                   tw_away=1, tw_home=1, tw_draw=1,
        #                                   dc_home_away=1, dc_home_draw=1,
        #                                   dc_away_draw=1, btts_yes=1,
        #                                   btts_no=1,
        #                                   over05=1,
        #                                   under05=1,
        #                                   over15=1,
        #                                   under15=1,
        #                                   over25=1,
        #                                   under25=1,
        #                                   over35=1,
        #                                   under35=1)
        #
        # self.game2 = PLGame.objects.create(home_team=home2, away_team=away2,
        #                                    tw_away=1, tw_home=1, tw_draw=1,
        #                                    dc_home_away=1, dc_home_draw=1,
        #                                    dc_away_draw=1, btts_yes=1,
        #                                    btts_no=1,
        #                                    over05=1,
        #                                    under05=1,
        #                                    over15=1,
        #                                    under15=1,
        #                                    over25=1,
        #                                    under25=1,
        #                                    over35=1,
        #                                    under35=1)
        #
        # self.game3 = PLGame.objects.create(home_team=home3, away_team=away3,
        #                                    tw_away=1, tw_home=1, tw_draw=1,
        #                                    dc_home_away=1, dc_home_draw=1,
        #                                    dc_away_draw=1, btts_yes=1,
        #                                    btts_no=1,
        #                                    over05=1,
        #                                    under05=1,
        #                                    over15=1,
        #                                    under15=1,
        #                                    over25=1,
        #                                    under25=1,
        #                                    over35=1,
        #                                    under35=1)
        #
        # self.result = PLResults.objects.create(game=self.game,
        #                                        tw_away=True, tw_home=True,
        #                                        tw_draw=True,
        #                                        dc_home_away=True,
        #                                        dc_home_draw=True,
        #                                        dc_away_draw=True,
        #                                        btts_yes=True,
        #                                        btts_no=True,
        #                                        over05=True,
        #                                        under05=True,
        #                                        over15=True,
        #                                        under15=True,
        #                                        over25=True,
        #                                        under25=True,
        #                                        over35=True,
        #                                        under35=True,
        #                                        evaluated=False)
        #
        # self.pool = GamePool.objects.create(sport=sport, pool_name='Support',
        #                                     is_supporters_pool=True,
        #                                     supporters_pool=home)

        self.user = User.objects.create_user(
            username='jacob', email='jacob@…', password='top_secret@!',
        )
        self.profile = Profile.objects.create(user=self.user,
                                              supported_team=home)

    def testLogin(self):
        self.client.login(username='jacob', password='top_secret@!')
        response = self.client.get(reverse('accounts:user_login'))
        self.assertEqual(response.status_code, 200)

    # def test_evaluated(self):
    #     """Game should be evaluated at creation of PLResults object"""
    #     game = PLResults.objects.get(game=self.game)
    #     self.assertTrue(game.evaluated)
    #
    # def test_was_updated_recently(self):
    #     """Test that the recently created object was updated recently."""
    #     game = PLResults.objects.get(game=self.game)
    #     self.assertTrue(game.was_updated_recently())
    #
    # def test_games_rendered_correctly(self):
    #     """Only the games still to come should be returned."""
    #     self.client.login(username='jacob', password='top_secret@!')
    #     user = User.objects.get(username='jacob')
    #     request = self.factory.get(reverse('epl'), follow=True)
    #     request.user = user
    #     response = append_bet(request)
    #     self.assertIn(str(self.game2), str(response.content))
    #     self.assertIn(str(self.game3), str(response.content))
    #     self.assertNotIn(str(self.game), str(response.content))

    def test_bank_account_balance_starts_at_1000(self):
        """Newly created user starts at 1000 units."""
        self.assertEqual(1000, self.profile.game_account_balance)


class TestTasks(TestCase):
    """Test celery tasks in accounts.tasks"""

    def test_whole_number_unchanged(self):
        """Whole numbers should not be changed"""
        self.assertEqual(fraction_to_float('12'), 12)

    def test_fractions_evaluated(self):
        self.assertEqual(float(3 / 4), fraction_to_float("3/4"))
