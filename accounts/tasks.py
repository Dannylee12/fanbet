from __future__ import absolute_import, unicode_literals
from datetime import datetime, timedelta
from celery import shared_task
import json
import logging
import hashlib
import time
import re

from django.db.utils import IntegrityError
from django.utils import timezone
from django.core.exceptions import ObjectDoesNotExist

import requests
from bs4 import BeautifulSoup
import pytz

from accounts.models import (Transaction, PLResults, PLGame, Profile, BetForm,
                             GamePool, User, Team, Pools)

logging.basicConfig(level='INFO')
logger = logging.getLogger()


@shared_task
def get_new_pool_rankings():
    """
    Async job to create a new row for each pool.
    Should run on a schedule, weekly after all users are paid.
    """
    for pool in GamePool.objects.all():
        logger.info(f"Ordering pool: {pool}")
        # Get users in this pool
        users = Profile.objects.filter(pools=pool).order_by('-game_account_accrued_balance')  # noqa
        for i, user in enumerate(users):
            logger.info(f"Creating row with {pool}, {user}, {i+1} "
                        f"for time {timezone.now()}")
            Pools.objects.create(pool_name=pool, member=user, rank=i+1,
                                 accrued_monies=user.game_account_accrued_balance,  # noqa
                                 date=timezone.now())


@shared_task
def evaluate_bet_results(instance: PLResults) -> bool:
    """
    When triggered, this will evaluate all transactions that contain the
    PLResult.
    :return: True if completed.
    :rtype: bool
    """
    # for instance in PLResults.objects.filter(evaluated=False):
    game_passed(instance)
    logger.info(f"Instance: {instance}")
    # Get all betforms that have this game in them
    betforms = BetForm.objects.filter(game=instance.game)
    for betform in betforms:
        # Group by Transaction
        transaction = betform.transaction
        win = True
        forms = BetForm.objects.filter(transaction=betform.transaction)
        for form in forms:
            logger.info(f"Form: {form}")
            if not form.game.passed:
                logger.info(f"all games not finished: {form.game}")
                win = False
                break
            transaction.status = 'DONE'
            transaction.save()
            result = PLResults.objects.get(game=form.game)
            if not (getattr(result, form.bet)):
                logger.info("result not true")
                win = False
                break
            form.bet_passed = True
            form.save()
        if win:
            logger.info("Winner! Pay winners.")
            pay_winners(transaction)

        instance.set_evaluated()
        return True


def game_passed(instance):
    """
    Log that the game has passed.
    :param instance: PLResult object
    :return: None
    """
    game = instance.game
    game.passed = True
    game.save()


def pay_winners(transaction):
    """
    Pay winners if their bets are all winners.
    :param transaction: winning transactions.
    :return: None
    """
    profile = Profile.objects.get(user=transaction.user.user)
    profile.game_account_accrued_balance += transaction.stand_to_win
    profile.save()


@shared_task
def order_global_pool():
    """
    Async task to order the winners in the global pool and save their
    rankings in their model.
    :return: True if finished
    """
    users = Profile.objects.order_by('-game_account_accrued_balance')
    for i, person in enumerate(users):
        person.global_ranking = i + 1
        person.save()
    return True


@shared_task
def order_supporter_pools():
    """
    Async job to order all supporter pools.
    :return: True if finished
    """
    supporter_pools = GamePool.objects.filter(is_supporters_pool=True)
    for supporter_pool in supporter_pools:
        for i, user in enumerate(
                Profile.objects.filter(supporter_pool=supporter_pool) \
                        .order_by('-game_account_accrued_balance')):
            user.supporter_pool_ranking = i + 1
            user.save()
    return True


@shared_task
def get_pool_rankings(username):
    """
    Async job to get all pools, order the profiles in these pools and update
    these user models.
    :return: True
    """
    user = Profile.objects.get(user=User.objects.get(username=username))
    pool_list = []
    for i, pool in enumerate(user.pools.filter(is_supporters_pool=False)):
        pool_object = Profile.objects.filter(pools=pool.id)
        pool_standing = list(pool_object.order_by('-game_account_balance')).index(user) + 1  # noqa
        members_in_pool = pool_object.all().count()
        pool_dict = {'pool_standing': pool_standing,
                     'pool_name': pool.pool_name,
                     'members_in_pool': members_in_pool}
        pool_list.append(pool_dict)
    user.pool_rankings = json.dumps(pool_list)
    user.save()
    return True


@shared_task
def pay_users():
    """
    Every Monday morning users get their R850, R1000 or R1150 based on how
    they performed the previous week.
    This is therefore run on a crontab.
    :return: True
    """
    for user in Profile.objects.all():
        weeks_profit = user.game_account_accrued_balance - user.end_of_previous_week_total  # noqa
        user.weeks_profit = weeks_profit
        # User got a special payment last week
        if user.previous_payment != 1000:
            user.game_account_balance += 1000
            user.previous_payment = 1000
        # User made more than R 1500
        elif user.weeks_profit >= 1500:
            user.game_account_balance += 1150
            user.previous_payment = 1150
        # User made less than R200
        elif user.weeks_profit < 200:
            user.game_account_balance += 850
            user.previous_payment = 850
        # Everyone else
        else:
            user.game_account_balance += 1000
            user.previous_payment = 1000
        # Move pool ranking to previous_week
        user.global_ranking_last_week = user.global_ranking
        user.supporter_pool_ranking_last_week = user.supporter_pool_ranking
        user.pool_rankings_last_week = user.pool_rankings
        # Save model
        user.save()
    return True


def fraction_to_float(fraction: str) -> float:
    """
    Returns a float from a fraction.
    :param fraction: fraction
    :return: number
    """
    try:
        if '/' not in fraction:
            return float(fraction)  # Whole Number
        return float(fraction.split('/')[0]) / float(fraction.split('/')[-1])
    except ValueError:
        return 0.0


@shared_task
def get_odds():
    """
    Get odds from oddschecker.com. Currently getting odds from bet365
    :return: None
    """
    logger.info("Running task get_odds")
    base_url = "https://www.oddschecker.com/"
    url = base_url + 'football/english/premier-league'

    r = requests.get(url)
    soup = BeautifulSoup(r.content, 'html.parser')

    trs = soup.find_all('tr', class_='match-on')

    for x in trs:
        try:
            home_team = x.contents[1].contents[0].contents[0]
            away_team = x.contents[1].contents[1].contents[0]
            logger.info(f"home_team: {home_team}")
            logger.info(f"away_team: {away_team}")
        except IndexError:
            continue
        try:
            ht = Team.objects.get(team_name__startswith=home_team)
        except ObjectDoesNotExist:
            if home_team == 'Man Utd':
                ht = Team.objects.get(team_name='Manchester United')
            elif home_team == 'Man City':
                ht = Team.objects.get(team_name='Manchester City')
            elif home_team == 'Bournemouth':
                ht = Team.objects.get(team_name='AFC Bournemouth')
            elif home_team == 'Wolves':
                ht = Team.objects.get(team_name='Wolverhampton Wanderers')
        try:
            at = Team.objects.get(team_name__startswith=away_team)
        except ObjectDoesNotExist:
            if away_team == 'Man Utd':
                at = Team.objects.get(team_name='Manchester United')
            elif away_team == 'Man City':
                at = Team.objects.get(team_name='Manchester City')
            elif away_team == 'Bournemouth':
                at = Team.objects.get(team_name='AFC Bournemouth')
            elif home_team == 'Wolves':
                at = Team.objects.get(team_name='Wolverhampton Wanderers')
        # Winner + Date of Game
        u = base_url + x.contents[5].contents[0].attrs['href']
        w = requests.get(u)
        soup_winner = BeautifulSoup(w.content, 'html.parser')
        current_date = datetime.now()
        for suffix in ("st", "nd", "rd", "th"):
            try:
                date = datetime.strptime(
                    soup_winner.find("span", class_='date').contents[0],
                    f"%A %d{suffix} %B / %H:%M")
                if current_date.month in [11, 12] and date.month in [1, 2]:
                    date = date.replace(year=current_date.year + 1,
                                        minute=date.minute + 1)
                else:
                    date = date.replace(year=current_date.year,
                                        minute=date.minute + 1)
                season = f"{date.year}/{date.year + 1}" if date.month > 5 else f"{date.year - 1} / {date.year}"
                logger.info(f"Season of game: {season}")
                pst = pytz.timezone('Europe/London')
                date = pst.localize(date).astimezone(pytz.utc)
            except ValueError:
                pass
        # Create unique number based on hash of Home Team + Away Team
        try:
            id_ = int(
                hashlib.sha256((str(ht) + str(at) + str(season)).encode('utf-8')).hexdigest(),
                16) % 10 ** 8
        except UnboundLocalError as e:  # ht or at not defined
            logger.error("Either at or ht is not defined.")
            logger.error(e.with_traceback(e.__traceback__))
            continue
        logger.info(f"id_: {id_}")
        logger.info(f"Home team: {ht}\nAway team: {at}")

        home_odds = \
        soup_winner.find('tr', {'data-bname': home_team}).contents[1].attrs[
            'data-o']
        away_odds = \
        soup_winner.find('tr', {'data-bname': away_team}).contents[1].attrs[
            'data-o']
        draw_odds = \
        soup_winner.find('tr', {'data-bname': "Draw"}).contents[1].attrs[
            'data-o']
        if not all([home_odds, away_odds, draw_odds]):
            logger.error("No odds to get for this game. Breaking from loop.")
            break
        # BTTS
        b_url = "/".join(
            u.split('/')[:-1])  # Remove /winner from url to get base url
        b = requests.get(b_url + "/both-teams-to-score")
        soup_btts = BeautifulSoup(b.content, 'html.parser')
        btts_yes = \
        soup_btts.find('tr', {'data-bname': 'Yes'}).contents[1].attrs['data-o']
        btts_no = soup_btts.find('tr', {'data-bname': 'No'}).contents[1].attrs[
            'data-o']
        # Double Chance
        d = requests.get(b_url + '/double-chance')
        soup_dc = BeautifulSoup(d.content, 'html.parser')
        home_draw_odds = \
        soup_dc.find('tr', {'data-bname': f'{home_team}-Draw'}).contents[
            1].attrs['data-o']
        away_draw_odds = \
        soup_dc.find('tr', {'data-bname': f'{away_team}-Draw'}).contents[
            1].attrs['data-o']
        home_away_odds = soup_dc.find('tr', {
            'data-bname': f'{home_team}-{away_team}'}).contents[1].attrs[
            'data-o']
        # Over/under Goals
        g = requests.get(b_url + '/total-goals-over-under')
        soup_goals = BeautifulSoup(g.content, 'html.parser')
        overs = []
        unders = []
        for y in range(4):
            overs.append(soup_goals.find('tr',
                                         {'data-bname': f'Over {y}.5'})
                         .contents[1].attrs['data-o'])
            unders.append(soup_goals.find('tr',
                                          {'data-bname': f'Under {y}.5'})
                          .contents[1].attrs['data-o'])
        try:
            game = PLGame.objects.get(pk=id_)
            logger.info(f"Updating {game}")
            if game.date != date and game.date != date - timedelta(minutes=1):
                logger.info("Game Date: " + str(game.date))
                logger.info("date: " + str(date))
                logger.error("Game dates do not match!")
                continue
            game.tw_home = fraction_to_float(home_odds)
            game.tw_away = fraction_to_float(away_odds)
            game.tw_draw = fraction_to_float(draw_odds)
            game.btts_yes = fraction_to_float(btts_yes)
            game.btts_no = fraction_to_float(btts_no)
            game.dc_home_away = fraction_to_float(home_away_odds)
            game.dc_home_draw = fraction_to_float(home_draw_odds)
            game.dc_away_draw = fraction_to_float(away_draw_odds)
            game.over05 = fraction_to_float(overs[0])
            game.over15 = fraction_to_float(overs[1])
            game.over25 = fraction_to_float(overs[2])
            game.over35 = fraction_to_float(overs[3])
            game.under05 = fraction_to_float(unders[0])
            game.under15 = fraction_to_float(unders[1])
            game.under25 = fraction_to_float(unders[2])
            game.under35 = fraction_to_float(unders[3])
            game.season = season
            game.save()
        except ObjectDoesNotExist:
            game = PLGame.objects.create(pk=id_, home_team=ht, away_team=at,
                                         tw_home=fraction_to_float(home_odds),
                                         tw_away=fraction_to_float(away_odds),
                                         tw_draw=fraction_to_float(draw_odds),
                                         btts_yes=fraction_to_float(btts_yes),
                                         btts_no=fraction_to_float(btts_no),
                                         dc_home_away=fraction_to_float(
                                             home_away_odds),
                                         dc_home_draw=fraction_to_float(
                                             home_draw_odds),
                                         dc_away_draw=fraction_to_float(
                                             away_draw_odds),
                                         over05=fraction_to_float(overs[0]),
                                         over15=fraction_to_float(overs[1]),
                                         over25=fraction_to_float(overs[2]),
                                         over35=fraction_to_float(overs[3]),
                                         under05=fraction_to_float(unders[0]),
                                         under15=fraction_to_float(unders[1]),
                                         under25=fraction_to_float(unders[2]),
                                         under35=fraction_to_float(unders[3]),
                                         date=date - timedelta(minutes=1))
            logger.info(game)
            game.save()
    return True


@shared_task
def get_results():
    """
    Scrape results from sky sports website.
    :return: None
    """
    logger.info("Running task get_results")
    url = 'http://www.skysports.com/premier-league-results'

    r = requests.get(url)
    r.raise_for_status()

    soup = BeautifulSoup(r.content, 'html.parser')

    matches = soup.find_all('div', class_='fixres__item')

    for match in matches:
        a = re.match(r'\s+([A-Za-z\s]+)\s+(\d+)'
                     r'\s+(\d+)\s+\d+:\d+\s+'
                     r'([A-Za-z\s]+)\s+([A-Z]+)', match.contents[1].text)
        if a:  # Regex match
            # Results page has Brighton and Hove Albion. DB has Brighton & Hove
            home_team = a.group(1).strip().replace('and', '&')  # Arsenal
            home_score = int(a.group(2))  # 2
            away_score = int(a.group(3))  # 4
            away_team = a.group(4).strip().replace('and', '&')  # Swansea City
            game_status = a.group(5)  # FT
            if home_team == 'Bournemouth':
                home_team = 'AFC Bournemouth'
            elif away_team == 'Bournemouth':
                away_team = 'AFC Bournemouth'
        else:
            logger.error("Game result failed")
            continue

        if game_status == 'FT':  # Game has finished
            id_ = int(hashlib.sha256(
                (home_team + away_team).encode('utf-8')).hexdigest(),
                      16) % 10 ** 8
            try:
                tw_home = False
                tw_away = False
                tw_draw = False
                dc_home_draw = False
                dc_home_away = False
                dc_away_draw = False
                over05 = False
                under05 = False
                over15 = False
                under15 = False
                over25 = False
                under25 = False
                over35 = False
                under35 = False
                btts_yes = False
                btts_no = False
                # Three ways
                if home_score > away_score:
                    tw_home = True
                elif away_score > home_score:
                    tw_away = True
                elif home_score == away_score:
                    tw_draw = True
                # Double Chance
                if home_score >= away_score:
                    dc_home_draw = True
                if away_score >= home_score:
                    dc_away_draw = True
                if away_score > home_score or away_score < home_score:
                    dc_home_away = True
                if away_score > 0 and home_score > 0:
                    btts_yes = True
                else:
                    btts_no = True
                # Over/Under goals
                total_goals = away_score + home_score
                if total_goals > 0.5:
                    over05 = True
                else:
                    under05 = True
                if total_goals > 1.5:
                    over15 = True
                else:
                    under15 = True
                if total_goals > 2.5:
                    over25 = True
                else:
                    under25 = True
                if total_goals > 3.5:
                    over35 = True
                else:
                    under35 = True
                game = PLGame.objects.get(pk=id_)
                logger.info(f"Game found: {game} with id: {id_}")
                game.passed = True
                game.save()
                result = PLResults.objects.create(
                    game=game,
                    tw_home=tw_home,
                    tw_away=tw_away,
                    tw_draw=tw_draw,
                    dc_home_draw=dc_home_draw,
                    dc_home_away=dc_home_away,
                    dc_away_draw=dc_away_draw,
                    over05=over05,
                    under05=under05,
                    over15=over15,
                    under15=under15,
                    over25=over25,
                    under25=under25,
                    over35=over35,
                    under35=under35,
                    btts_yes=btts_yes,
                    btts_no=btts_no)
            except ObjectDoesNotExist as e:
                logger.error(f"{str(home_team)} vs {str(away_team)} not found "
                             f"in models.")
                logger.error(f"ID: {id_}")
                logger.error(e.with_traceback(e.__traceback__))
                logger.critical("Catastrophic error. Game not found.")
                continue
            except IntegrityError:
                logger.info(f"Result {str(home_team)} vs {str(away_team)} "
                            f"already exists. Breaking")
                break

    return True
