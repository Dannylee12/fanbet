from datetime import datetime
import json
import logging
import pytz

from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse, \
    HttpResponseForbidden, HttpResponseNotAllowed
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.utils import timezone
from accounts.models import User, Profile, PLGame, Transaction, BetForm, GamePool
from django.http import HttpResponse
from django.shortcuts import render, redirect
from accounts.forms import UserRegistrationForm, UserLoginForm
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from accounts.tokens import account_activation_token
from django.contrib.auth.models import User
from django.core.mail import EmailMessage
from django.contrib.auth.forms import SetPasswordForm

from tabs.views import create_pool_list

logging.basicConfig(level='INFO')
logger = logging.getLogger()


def forgot_password(request):
    user_valid, email_sent, email_address = True, False, None
    if request.method == 'POST':
        current_site = get_current_site(request)
        email_address = request.POST.get("email")
        try:
            user = User.objects.get(email=email_address, is_active=True)
        except ObjectDoesNotExist:
            user_valid = False
        else:
            mail_subject = 'Reset your Puntdoctor password.'
            message = render_to_string('accounts/forgot_password_email.html', {
                'user': user,
                'domain': current_site.domain,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'token': account_activation_token.make_token(user),
            })
            to_email = email_address
            email = EmailMessage(
                mail_subject, message, to=[to_email]
            )
            logger.info(f"Sending email: {message} to {to_email}")
            email.send()
            email_sent = True
    return render(request, 'accounts/password_reset_email.html',
                  {'email_sent': email_sent, 'user_valid': user_valid,
                   'email_address': email_address})


def set_password(request, uidb64, token):
    """Update password based on email."""
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if not user:
        return HttpResponseForbidden()  # Just straight up forbid this request, looking fishy already!
    if request.method == 'POST':
        form = SetPasswordForm(user, data=request.POST)
        if form.is_valid():
            form.save()
            return redirect('index')
            # TODO: toastr message that password has been changed.
    elif request.method == 'GET':
        form = SetPasswordForm(user)
    else:
        return HttpResponseNotAllowed(permitted_methods=['GET', 'POST'])
    form.fields['new_password2'].label = 'Confirm New Password'
    form.fields['new_password2'].longest = True
    return render(request, 'accounts/password_reset.html',
                  {'form': form, 'type': 'reset', 'uid': uid, 'token': token})


def signup(request):
    user_login_form = UserLoginForm(prefix='login')
    email_sent = False
    password_incorrect = False
    if request.method == 'POST':

        if request.POST.get("login"):
            username = request.POST.get('login-username')
            password = request.POST.get('login-password')

            user = authenticate(username=username, password=password)

            # If we have a user
            if user:
                # Check it the account is active
                if user.is_active:
                    # Log the user in.
                    login(request, user)
                    # Send the user back to some page.
                    # In this case their homepage.
                    return HttpResponseRedirect(reverse('index'))
            else:
                password_incorrect = True
                user_registration_form = UserRegistrationForm()

        elif request.POST.get("register"):  # Register
            user_registration_form = UserRegistrationForm(request.POST)
            if user_registration_form.is_valid():
                user = user_registration_form.save(commit=False)
                user.is_active = False
                user.save()
                current_site = get_current_site(request)
                mail_subject = 'Activate your Puntdoctor account.'
                message = render_to_string('accounts/acc_active_email.html', {
                    'user': user,
                    'domain': current_site.domain,
                    'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                    'token': account_activation_token.make_token(user),
                })
                to_email = user_registration_form.cleaned_data.get('email')
                email = EmailMessage(
                            mail_subject, message, to=[to_email]
                )
                logger.info(f"Sending email: {message} to {to_email}")
                email.send()
                email_sent = True

                return render(request, 'accounts/registration.html',
                              {'user_registration_form': user_registration_form,
                               'user_login_form': user_login_form,
                               'email_sent': email_sent})
    else:
        user_registration_form = UserRegistrationForm()

    return render(request, 'accounts/registration.html',
                  {'user_registration_form': user_registration_form,
                   'user_login_form': user_login_form,
                   'password_incorrect': password_incorrect})


def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        profile = Profile.objects.create(user=user)
        profile.pools.add(GamePool.objects.get(pk=46))  # Join Global Rankings
        profile.save()
        # TODO: Login with login(request, user, backend)
        return redirect('index')
    else:
        return HttpResponse('Activation link is invalid!')


def health_check(request):
    """Returns 200 for a health check."""
    return HttpResponse("<h4>Healthy</h4>")


def index(request):
    return render(request, 'accounts/index.html')


def validate_username(request):
    """Ajax call to check whether a given username is taken."""
    username = request.GET.get('username', None)
    data = {
        'is_taken': User.objects.filter(username__iexact=username).exists()
    }
    return JsonResponse(data)


@login_required
def account_view(request):
    if request.user.is_authenticated():
        try:
            user = request.user
            profile = Profile.objects.get(user=request.user.id)
        except NameError:
            profile = 'n/a'
    return render(request, 'accounts/account.html', {'profile': profile,
                                                     'user': user})


@login_required
def pool_rankings(request):
    """
    Method to return pool rankings to the left-sidebar.html page.
    This method should be called in any view that requires the pool rankings.
    :param request:
    :return: None
    """
    try:
        # Global Pool
        user = Profile.objects.get(user=request.user.id)
        total_members = Profile.objects.all().count()
        global_total = f"{user.global_ranking}/{total_members}"
        # Supporters Pool
        # Get the number of users in the Supporters Pool
        total_supporters = Profile.objects.filter(supporter_pool=
                                                  user.supporter_pool).count()
        supporter_total = f"{user.supporter_pool_ranking}/{total_supporters}"
        # For all other pools, get the rankings are in a list
        pool_rankings = json.loads(user.pool_rankings)
        pool_rankings_last_week = json.loads(user.pool_rankings_last_week)
        pool_rankings_updated = []
        for pr, prlw in zip(pool_rankings, pool_rankings_last_week):
            combined = {'pool_movement': prlw['pool_standing'] - pr['pool_standing']}
            pool_rankings_updated.append({**pr, **combined})
    except Exception as e:
        logger.error("Can't rank pools in sidebar.")
        logger.error(e.with_traceback(e.__traceback__))
        global_total = supporter_total = pool_rankings_updated = None
        user.global_ranking_last_week = user.global_ranking = 0
        user.supporter_pool_ranking_last_week = user.supporter_pool_ranking = 0

    return {'global_total': global_total,
            'global_movement': user.global_ranking_last_week -
                               user.global_ranking,
            'supporter_total': supporter_total,
            'supporter_movement': user.supporter_pool_ranking_last_week -
                                  user.supporter_pool_ranking,
            'supporter_pool': user.supporter_pool,
            'pool_rankings': pool_rankings_updated}


def append_bet(request, season='2018/2019'):
    """
    This is the epl view. Need to rename this view.
    :param request:
    :return:
    """
    # TODO: accept season argument and add season to page
    pool_rankings = {}  # give default value
    # Get pools rankings from pool_rankings()
    try:
        user = Profile.objects.get(user=request.user.id)
        pool_rankings = create_pool_list(request)
        if int(user.game_account_balance) > 0:
            insufficient_funds = False
        else:
            insufficient_funds = True
    except ObjectDoesNotExist:
        insufficient_funds = True
        pass
    games_form = PLGame.objects.filter(passed=False,
                                       date__gt=timezone.now(),
                                       season=season).order_by('date')

    set = False

    if request.is_ajax():
        # Get all variables from ajax call
        stand_to_win = request.POST.get('stw')
        games = request.POST.get('games').split(',')

        for game in games:
            game_model = PLGame.objects.get(id=game)
            if game_model.date < \
                    datetime.now(tz=pytz.timezone('Africa/Johannesburg')):
                raise ValueError("Game already Started.")

        amount = request.POST.get('amount')
        amount = float(amount)
        bets = request.POST.get('markets').split(',')
        odds = request.POST.get('odds')
        all_odds = request.POST.get('all_odds').split(',')
        # Check that user has enough money to bet
        # Should be caught by javascript but check just in case.
        if user.game_account_balance >= amount:
            # Make a bet - Create a transaction
            user.game_account_balance -= amount
            user.save()
            transaction = Transaction(user=user, stand_to_win=stand_to_win,
                                      odds=odds, status='PENDING')
            transaction.save()
            # Create a betform for each bet and game
            for bet, game, odd in zip(bets, games, all_odds):
                game = PLGame.objects.get(id=game)
                bet_transaction = BetForm.objects.create(
                    transaction=transaction, user=user, game=game, bet=bet,
                    bet_placed=True, odds=float(odd), status='PENDING')
                bet_transaction.save()
        else:
            raise ValueError("Not enough money in account.")

    return render(request, 'accounts/epl.html',
                  {'games': games_form, 'set': set,
                   'insufficient_funds': insufficient_funds,
                   'pools_sidebar': pool_rankings})


@login_required
def transactions(request):
    """
    Render all transactions that the logged in user has placed.
    :param request: 
    :return: a list of dictionaries containing transactions with betforms
    enclosed.
            i.e. [{transaction: {stand_to_win: x, status: y,
                  betform: {game: z, bet: a, odds: b, status: c}]
    """
    try:
        user = Profile.objects.get(user=request.user.id)
        trans = Transaction.objects.filter(user=user).order_by('-updated')
        transactions_to_render = []
        for tran in trans:
            try:
                transactions_to_render.append({'stand_to_win': tran.stand_to_win,
                    'status': tran.status,
                    'betform': BetForm.objects.filter(transaction=tran)})
            except AttributeError as e:
                logger.error(e.with_traceback(e.__traceback__))
    except ObjectDoesNotExist:
        transactions_to_render = []

    return render(request, 'accounts/transactions.html',
                  {'transactions_to_render': transactions_to_render})


@login_required
def user_logout(request):
    # Log out the user.
    logout(request)
    # Return to homepage.
    return HttpResponseRedirect(reverse('index'))


def user_login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)

        # If we have a user
        if user:
            # Check it the account is active
            if user.is_active:
                # Log the user in.
                login(request, user)
                # Send the user back to some page.
                # In this case their homepage.
                return HttpResponseRedirect(reverse('index'))
            else:
                # If account is not active:
                return HttpResponse("Your account is not active.")
        else:
            logger.info("Someone tried to login and failed.")
            logger.info(f"They used username: {username} and password: "
                        f"{password}")
            return HttpResponseRedirect(reverse('accounts:register'))

    else:
        # Nothing has been provided for username or password.
        return render(request, 'accounts/login.html', {})
