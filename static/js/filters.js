// Round Currency Values
function roundNumberToTwo(value) {
    if (!value) {
        value = 0;
    }
    value = value.toFixed(2);
    return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}