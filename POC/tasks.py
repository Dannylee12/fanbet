import time

from celery import Celery

app = Celery('tasks', backend='rpc://',
             broker='redis://celery-redis-queue:6379/0')
# app.conf.broker_url = 'redis://:VYgE3XPB@35.187.24.210:6379/0'


@app.task
def add(x, y):
    return x + y
