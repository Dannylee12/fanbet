"""POC URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from accounts.views import (index, user_logout, account_view, health_check,
                            transactions, append_bet, validate_username)

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^_ah/health', health_check, name='health_check'),
    url(r'^_ah/vm_health', health_check, name='health_check1'),
    url(r'^$', append_bet, name='index'),
    url(r'^accounts/', include('accounts.urls')),
    url(r'^logout/$', user_logout, name='logout'),
    url(r'^epl/$', append_bet, name='epl'),
    url(r'^account/$', account_view, name='account'),
    url(r'^transactions/$', transactions, name='transactions'),
    url(r'tabs/', include('tabs.urls')),
]
