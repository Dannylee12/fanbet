# PuntDoctor
---

## Cloud Postgres database

This website will use a postgres instance running in the cloud.
mane: models-db
Username: admin
Password: St@k3Br0

### Proxy
cloud_sql_proxy is installed so that we can run in local dev mode while accessing
the data stored in the cloud.

If this is not installed, it can be installed with:
```
curl -o cloud_sql_proxy https://dl.google.com/cloudsql/cloud_sql_proxy.darwin.amd64
chmod +x cloud_sql_proxy
```
Then the command to connect to our instance is:
```
./cloud_sql_proxy -instances="fanbet-cloud:europe-west1:models-db"=tcp:5432
```

### Settings.py

In our settings.py, the database connection should be as follows:

```
# [DB CONFIG]
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'Models',
        'USER': 'admin',
        'PASSWORD': 'St@k3Br0',
    }
}
# In the flexible environment, you connect to CloudSQL using a unix socket.
# Locally, you can use the CloudSQL proxy to proxy a localhost connection
# to the instance
DATABASES['default']['HOST'] = '/cloudsql/fanbet-cloud:europe-west1:models-db'
if os.getenv('GAE_INSTANCE'):
    pass
else:
    DATABASES['default']['HOST'] = '127.0.0.1'
# [END dbconfig]
```

## Static files
Static files need to be synced to a cloud storage bucket.

this bucket is `gs://static-cloud/static`


### Deploy
Therefore, to deploy to *staging*:
Run
```
./manage.py collectstatic
gsutil rsync -R static/ gs://static-cloud/static
gcloud app deploy
```

