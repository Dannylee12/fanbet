from django.db import models

# Create your models here.


class WishList(models.Model):
    """
    Place to store wishlist items in the database.
    """
    header = models.CharField(max_length=20)
    text = models.CharField(max_length=1000)
    date_added = models.DateTimeField(auto_now_add=True)
    viewed = models.BooleanField(default=False)

    def __str__(self):
        return self.header + " Note this has a date_added field"
