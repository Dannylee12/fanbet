from django.shortcuts import render
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.decorators import login_required
from django.db.models.query import QuerySet
from django.http import request

import json

from tabs.forms import WishListForm
from accounts.models import GamePool, Profile, Pools


@login_required
def pools_view(request):
    """
    Show all pools availible to join.
    Join availible pools, create pools if they don't exist.
    :param request: http request
    :return: render view in html template (pools.html)
    """
    if request.is_ajax():
        return render_pools(request)

    pool_list = create_pool_list(request)

    return render(request, 'tabs/pools.html', {'pools': pool_list})


def create_pool_list(request: request) -> list:
    """Return a list of pools with the members ranking and movements"""
    #  Get a list of all pools

    try:
        user = Profile.objects.get(user=request.user.id)
    except (ObjectDoesNotExist, NameError):
        return []

    pools = []
    pools = user.pools.all()
    if not pools:
        return pools

    pool_list = []
    for pool in pools:
        rank = list(Pools.objects.filter(pool_name=pool, member=user).order_by(
            '-date').values_list('rank', flat=True)[:2])
        standing = 0
        try:
            standing = rank[0]
            movement = rank[1] - rank[0]
        except IndexError:
            movement = 0
        pool_dict = {'pool_standing': standing,
                     'members_in_pool': Pools.objects.filter(
                         pool_name=pool).order_by().values_list('member',
                                                                flat=True).distinct().count(),
                     'pool_name': pool,
                     'pool_movement': movement,
                     'pk': pool.pk}
        pool_list.append(pool_dict)

    return pool_list


def render_pools(request):
    if request.is_ajax():
        pass


@login_required
def pool_view(request, pool_id=None):
    """
    Render the current pool rankings and the graph showing movement.
    :param request:
    :param pool_id: id of the pool to render.
    :return:
    """
    pool_name = GamePool.objects.get(pk=pool_id)
    members = Pools.objects.filter(pool_name=pool_name).order_by().values_list('member', flat=True).distinct()
    pool_ranking = Pools.objects.filter(pool_name=pool_name).order_by('-date', 'accrued_monies')
    # take the top x members from the pool and reverse the order.
    latest_rankings = list(pool_ranking)[:len(members)][::-1]

    rankings_list = {}
    for member in members:
        rankings_list[str(Profile.objects.get(pk=member))] = list(Pools.objects.values('rank', 'date').filter(member=member, pool_name=pool_name).order_by('date'))

    parse_dates(rankings_list)
    dates = Pools.objects.filter(pool_name=pool_name).order_by('date').values_list('date', flat=True).distinct()
    try:
        min_dates = str(min(dates))
        max_dates = str(min(dates))
    except ValueError:
        return render(request, 'tabs/pool.html', {'no_rankings': True})

    return render(request, 'tabs/pool.html', {"pool_ranking": latest_rankings,
                                              "rankings": rankings_list,
                                              "dates": {"min": min_dates,
                                                        "max": max_dates}}
                  )


def parse_dates(rankings_object):
    """
    Format all datetime.date objects in the rankings object
    Updates the dictionary in place.
    :param rankings_object: {'tom':
    [{'rank': 2, 'date': datetime.date(2018, 2, 17)}, {'rank': 2,
    'date': datetime.date(2018, 4, 9)}, {'rank': 1, 'date': datetime.date(...
    :return: dates in the format var format = d3.time.format("%Y-%m-%d");
    """
    for member, result in rankings_object.items():
        for row in result:
            row['date'] = str(row['date'])


def just_the_tip(request):
    return render(request, 'tabs/just-the-tip.html')


def how_it_works(request):
    return render(request, 'tabs/how-it-works.html')


def prizes(request):
    return render(request, 'tabs/prizes.html')


def pools(request):
    return render(request, 'tabs/pools.html')


def registration_modal(request):
    return render(request, 'tabs/registration-modal.html')


def wishlist(request):
    """
    Submit wishlist items to the database to be read by us.
    """
    submitted = False

    if request.method == 'POST':
        wish_form = WishListForm(data=request.POST)

        if wish_form.is_valid():
            wish_form.save()
            submitted = True

    else:
        wish_form = WishListForm()

    return render(request, 'tabs/wishlist.html', {
        'wish_form': wish_form, 'submitted': submitted
    })
