from django import forms
from tabs.models import WishList


class WishListForm(forms.ModelForm):
    text = forms.CharField(widget=forms.TextInput())

    class Meta:
        model = WishList
        fields = ('header', 'text')