from django.conf.urls import url
from tabs import views

# SET THE NAMESPACE!
app_name = 'tabs'

# Be careful setting the name to just /login use userlogin instead!
urlpatterns = [
    url(r'^just-the-tip/$', views.just_the_tip, name='just-the-tip'),
    url(r'^wishlist/$', views.wishlist, name='wishlist'),
    url(r'^how-it-works/$', views.how_it_works, name='how-it-works'),
    url(r'^prizes/$', views.prizes, name='prizes'),
    url(r'^pools/$', views.pools_view, name='pools'),
    url(r'^pools/pool/(?P<pool_id>\d+$)', views.pool_view, name='pool')
]
