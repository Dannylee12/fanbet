"""Get results on a daily basis."""
import hashlib
import re
import requests
from bs4 import BeautifulSoup

from accounts.models import PLResults

url = 'http://www.skysports.com/premier-league-results'

r = requests.get(url)
r.raise_for_status()

soup = BeautifulSoup(r.content, 'html.parser')

matches = soup.find_all('div', class_='fixres__item')

for match in matches:
    a = re.match(r'\s+([A-Za-z\s]+)\s+(\d+)'
                 r'\s+(\d+)\s+\d+:\d+\s+'
                 r'([A-Za-z\s]+)\s+([A-Z]+)', match.contents[1].text)
    if a:  # Regex match
        # Results page has Brighton and Hove Albion. DB has Brighton & Hove
        home_team = a.group(1).strip().replace('and', '&')  # Arsenal
        home_score = int(a.group(2))  # 2
        away_score = int(a.group(3))  # 4
        away_team = a.group(4).strip().replace('and', '&')  # Swansea City
        game_status = a.group(5)  # FT
    else:
        print("Game result failed")
        continue

    if game_status == 'FT':  # Game has finished
        id_ = int(hashlib.sha256((home_team + away_team).encode('utf-8')).hexdigest(),16) % 10 ** 8
        result = PLResults.objects.create(game=PL)
        # Three wayss
        if home_score > away_score:
            result.tw_home = True
        elif away_score > home_score:
            result.tw_away = True
        elif home_score == away_score:
            result.tw_draw = True
        # Double Chance
        if home_score >= away_score:
            result.dc_home_draw = True
        if away_score >= home_score:
            result.dc_away_draw = True
        if away_score > home_score or away_score < home_score:
            result.dc_home_away = True
        if away_score > 0 and home_score > 0:
            result.btts = True
        # Over/Under goals
        total_goals = away_score + home_score
        if total_goals > 0.5:
            result.over05 = True
        else:
            result.under05 = True
        if total_goals > 1.5:
            result.over15 = True
        else:
            result.under15 = True
        if total_goals > 2.5:
            result.over25 = True
        else:
            result.under25 = True
        if total_goals > 3.5:
            result.over35 = True
        else:
            result.under35 = True
