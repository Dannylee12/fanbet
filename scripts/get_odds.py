from __future__ import absolute_import, unicode_literals
from datetime import datetime, timedelta
from celery import shared_task
import json
import logging
import hashlib
import time
import re

import requests
from bs4 import BeautifulSoup
import pytz


logger = logging.getLogger(__name__)
logger.setLevel("INFO")


def get_odds():
    """
    Get odds from oddschecker.com. Currently getting odds from bet365
    :return: None
    """
    logger.info("Running task get_odds")
    base_url = "https://www.oddschecker.com/"
    url = base_url + 'football/english/premier-league'

    r = requests.get(url)
    soup = BeautifulSoup(r.content, 'html.parser')

    trs = soup.find_all('tr', class_='match-on')

    for x in trs:
        try:
            home_team = x.contents[1].contents[0].contents[0]
            away_team = x.contents[1].contents[1].contents[0]
            logger.info(f"home_team: {home_team}")
            logger.info(f"away_team: {away_team}")
            ht = home_team
            at = away_team
        except IndexError:
            continue
        # Create unique number based on hash of Home Team + Away Team
        try:
            id_ = int(
                hashlib.sha256((str(ht) + str(at)).encode('utf-8')).hexdigest(),
                16) % 10 ** 8
        except UnboundLocalError as e:  # ht or at not defined
            logger.error("Either at or ht is not defined.")
            logger.error(e.with_traceback(e.__traceback__))
            continue
        logger.info(f"id_: {id_}")
        logger.info(f"Home team: {ht}\nAway team: {at}")
        # Winner + Date of Game
        u = base_url + x.contents[5].contents[0].attrs['href']
        w = requests.get(u)
        soup_winner = BeautifulSoup(w.content, 'html.parser')
        for suffix in ("st", "nd", "rd", "th"):
            try:
                date = datetime.strptime(
                    soup_winner.find("span", class_='date').contents[0],
                    f"%A %d{suffix} %B / %H:%M")
                date = date.replace(year=2017 if date.month > 6 else 2018)
                # TODO: make this work more elegantly.
                date = date.replace(tzinfo=pytz.timezone('Europe/London'))
                break
                # date = pytz.timezone('Africa/Johannesburg').localize(date)
            except ValueError:
                pass
        home_odds = \
        soup_winner.find('tr', {'data-bname': home_team}).contents[1].attrs[
            'data-o']
        away_odds = \
        soup_winner.find('tr', {'data-bname': away_team}).contents[1].attrs[
            'data-o']
        draw_odds = \
        soup_winner.find('tr', {'data-bname': "Draw"}).contents[1].attrs[
            'data-o']
        if not all([home_odds, away_odds, draw_odds]):
            logger.error("No odds to get for this game. Breaking from loop.")
            break
        # BTTS
        b_url = "/".join(
            u.split('/')[:-1])  # Remove /winner from url to get base url
        b = requests.get(b_url + "/both-teams-to-score")
        soup_btts = BeautifulSoup(b.content, 'html.parser')
        btts_yes = \
        soup_btts.find('tr', {'data-bname': 'Yes'}).contents[1].attrs['data-o']
        btts_no = soup_btts.find('tr', {'data-bname': 'No'}).contents[1].attrs[
            'data-o']
        # Double Chance
        d = requests.get(b_url + '/double-chance')
        soup_dc = BeautifulSoup(d.content, 'html.parser')
        home_draw_odds = \
        soup_dc.find('tr', {'data-bname': f'{home_team}-Draw'}).contents[
            1].attrs['data-o']
        away_draw_odds = \
        soup_dc.find('tr', {'data-bname': f'{away_team}-Draw'}).contents[
            1].attrs['data-o']
        home_away_odds = soup_dc.find('tr', {
            'data-bname': f'{home_team}-{away_team}'}).contents[1].attrs[
            'data-o']
        # Over/under Goals
        g = requests.get(b_url + '/total-goals-over-under')
        soup_goals = BeautifulSoup(g.content, 'html.parser')
        overs = []
        unders = []
        for y in range(4):
            overs.append(soup_goals.find('tr',
                                         {'data-bname': f'Over {y}.5'})
                         .contents[1].attrs['data-o'])
            unders.append(soup_goals.find('tr',
                                          {'data-bname': f'Under {y}.5'})
                          .contents[1].attrs['data-o'])
        try:
            game = PLGame.objects.get(pk=id_)
            logger.info(f"Updating {game}")
            if game.date != date and game.date != date - timedelta(minutes=1):
                logger.info("Game Date: " + str(game.date))
                logger.info("date: " + str(date))
                continue
            game.tw_home = fraction_to_float(home_odds)
            game.tw_away = fraction_to_float(away_odds)
            game.tw_draw = fraction_to_float(draw_odds)
            game.btts_yes = fraction_to_float(btts_yes)
            game.btts_no = fraction_to_float(btts_no)
            game.dc_home_away = fraction_to_float(home_away_odds)
            game.dc_home_draw = fraction_to_float(home_draw_odds)
            game.dc_away_draw = fraction_to_float(away_draw_odds)
            game.over05 = fraction_to_float(overs[0])
            game.over15 = fraction_to_float(overs[1])
            game.over25 = fraction_to_float(overs[2])
            game.over35 = fraction_to_float(overs[3])
            game.under05 = fraction_to_float(unders[0])
            game.under15 = fraction_to_float(unders[1])
            game.under25 = fraction_to_float(unders[2])
            game.under35 = fraction_to_float(unders[3])
            game.save()
        except ObjectDoesNotExist:
            game = PLGame.objects.create(pk=id_, home_team=ht, away_team=at,
                                         tw_home=fraction_to_float(home_odds),
                                         tw_away=fraction_to_float(away_odds),
                                         tw_draw=fraction_to_float(draw_odds),
                                         btts_yes=fraction_to_float(btts_yes),
                                         btts_no=fraction_to_float(btts_no),
                                         dc_home_away=fraction_to_float(
                                             home_away_odds),
                                         dc_home_draw=fraction_to_float(
                                             home_draw_odds),
                                         dc_away_draw=fraction_to_float(
                                             away_draw_odds),
                                         over05=fraction_to_float(overs[0]),
                                         over15=fraction_to_float(overs[1]),
                                         over25=fraction_to_float(overs[2]),
                                         over35=fraction_to_float(overs[3]),
                                         under05=fraction_to_float(unders[0]),
                                         under15=fraction_to_float(unders[1]),
                                         under25=fraction_to_float(unders[2]),
                                         under35=fraction_to_float(unders[3]),
                                         date=date - timedelta(minutes=1))
            logger.info(game)
            game.save()
    return True

if __name__ == '__main__':
    get_odds()
