FROM gcr.io/google_appengine/python

# Create a virtualenv for the application dependencies.
# # If you want to use Python 3, add the -p python3.4 flag.
RUN virtualenv /env -p python3.6
ENV PATH /env/bin:$PATH

ADD requirements.txt /app/requirements.txt
RUN /env/bin/pip install -r /app/requirements.txt
ADD / /app

CMD gunicorn -b :$PORT POC.wsgi --timeout 300 --access-logfile '-' --error-logfile '-'
